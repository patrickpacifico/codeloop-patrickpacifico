Processo Seletivo - Web Developer  Terceira Etapa - Patrick Pacífico da Silva

Desenvolver um sistema básico de cadastramento de estudantes.

1. O usuário deve poder criar, editar e deletar alunos:

- No menu "Funções" -> "Cadastrar Novo Estudante" = (CRIAR)
- No menu "Funções" -> "Lista de Estudantes Cadastrados" -> "Lápis" = (EDITAR)
- No menu "Funções" -> "Lista de Estudantes Cadastrados" -> "Lixeira" = (DELETAR)
- No menu "Funções" -> "Lista de Estudantes Cadastrados" -> "Lápis" -> "Lixeira" = (DELETAR)

2. O usuário deve poder ver todos os alunos cadastrados:

- No menu "Funções" -> "Lista de Estudantes Cadastrados" = (ALL)

* OBS1: Para ingressar no 1° Ano o estudante deve ter no mínimo 6 anos!
* OBS2: A data de pagamento não poderá ser retroativa!
* OBS3: Após um CEP válido, a RUA, BAIRRO, CIDADE e ESTADO são preenchidos automaticamete!

Desenvolvido com:

- Laravel
- MySQL
- CSS 3, HTML e JavaScript
- BootStrap

git clone https://patrickpacifico@bitbucket.org/patrickpacifico/codeloop-patrickpacifico.git
