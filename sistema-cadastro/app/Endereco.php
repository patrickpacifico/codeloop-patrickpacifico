<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    protected $fillable = [
      'id_endereco',
      'cep',
      'rua',
      'numero',
      'complemento',
      'bairro',
      'cidade',
      'estado',
      'id_estudantes',
      'created_at',
      'update_at'
    ];

    protected $primaryKey = 'id_endereco';
    protected $table = 'enderecos';

    public fuction estudante()
    {
        return $this->belongsTo(Estudantes::class, 'id_estudantes');
    }
}
