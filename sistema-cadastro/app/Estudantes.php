<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudantes extends Model
{
    protected $fillable = [
      'id_estudantes',
      'nome',
      'data_nascimento',
      'serie_ingresso',
      'cep',
      'rua',
      'numero',
      'complemento',
      'bairro',
      'cidade',
      'uf',
      'nome_mae',
      'cpf_mae',
      'data_pagamento',
      'created_at',
      'update_at'
    ];

    protected $primaryKey = 'id_estudantes';
    protected $table = 'estudantes';

    //data formatada m-d-Y
    protected $appends = ['data2'];
    public function getData2Attribute()
    {
        return date('d-m-Y', strtotime($this->attributes['data_nascimento']));
    }

    protected $appends2 = ['data3'];
    public function getData3Attribute()
    {
        return date('d-m-Y', strtotime($this->attributes['data_pagamento']));
    }

}
