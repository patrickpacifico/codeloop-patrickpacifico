<?php

namespace App\Http\Controllers;

use App\Estudantes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EstudantesController extends Controller
{
    private $estudante;

    public function __construct()
    {
        $this->estudante = new Estudantes();
    }

    public function index()
    {
        $list_estudantes = Estudantes::all();
        return view('estudantes.index', [
          'estudantes' => $list_estudantes
        ]);
    }

    public function listarView()
    {
        $list_estudantes = Estudantes::all();
        return view('estudantes.listar', [
          'estudantes' => $list_estudantes
        ]);
    }

    public function novoView()
  {
      return view('estudantes.create');
  }

  public function store(Request $request)
  {
      $validacao = $this->validacao($request->all());
      if ($validacao->fails()){
          return redirect()->back()
                  ->withErrors($validacao->errors())
                  ->withInput($request->all());
      }

      Estudantes::create($request->all());
      return redirect('/listar')->with('message', 'Estudante criado com sucesso!');
  }

  public function excluirView($id_estudantes)
  {
      return view('estudantes.delete', [
          'estudantes' => $this->getEstudantes($id_estudantes)
      ]);
  }

  public function destroy($id_estudantes)
  {
      $this->getEstudantes($id_estudantes)->delete();

      return redirect(url('/'))->with('sucess', 'Estudante excluído!');
  }

  public function editarView($id_estudantes)
  {
      return view('estudantes.edit', [
          'estudantes' => $this->getEstudantes($id_estudantes)
      ]);
  }

  public function update(Request $request)
  {
      $validacao = $this->validacao($request->all());
      if ($validacao->fails()){
          return redirect()->back()
                  ->withErrors($validacao->errors())
                  ->withInput($request->all());
      }

      $estudantes = $this->getEstudantes($request->id);
      $estudantes->update($request->all());

      return redirect('/listar');
  }

    protected function getEstudantes($id_estudantes)
  {
       return $this->estudante->find($id_estudantes);
  }

  private function validacao($data)
  {
      $regras = [
        'nome' => 'required',
        'data_nascimento' => 'required',
        'serie_ingresso' => 'required',
        'cep' => 'required',
        'rua' => 'required',
        'numero' => 'required',
        'bairro' => 'required',
        'cidade' => 'required',
        'uf' => 'required',
        'nome_mae' => 'required',
        'cpf_mae' => 'required',
        'data_pagamento' => 'required'
      ];

      $mensagem = [
        'nome.required' => 'Campo nome é obrigatório',
        'data_nascimento.required' => 'Campo data de nascimento é obrigatório',
        'serie_ingresso.required' => 'Campo serie de ingresso é obrigatório',
        'cep.required' => 'Campo CEP é obrigatório',
        'nome_mae.required' => 'Campo nome da mãe é obrigatório',
        'cpf_mae.required' => 'Campo CPF da mãe é obrigatório',
        'data_pagamento.required' => 'Campo data de pagamento é obrigatório',
      ];

      return Validator::make($data, $regras, $mensagem);
  }
}
