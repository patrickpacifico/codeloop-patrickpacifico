<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabelaEstudantes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudantes', function (Blueprint $table) {
            $table->increments('id_estudantes');
            $table->string('nome', 100);
            $table->date('data_nascimento');
            $table->string('serie_ingresso', 1);
            $table->string('cep', 9);
            $table->string('rua', 120);
            $table->integer('numero');
            $table->string('complemento', 50);
            $table->string('bairro', 50);
            $table->string('cidade', 50);
            $table->string('uf', 2);
            $table->string('nome_mae', 100);
            $table->string('cpf_mae', 11);
            $table->date('data_pagamento');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudantes');
    }
}
