@extends ("template.app")

@section("content")
<div class="col-md-12">
    <h3>Cadastrar <strong>Novo Estudante</strong></h3>
</div>
<div class="row bg-light rounded-circle p-3">
    <div class="col-md-6 p-0">
      <form action="{{ url('/store') }}" method="POST">
        {{ csrf_field() }}
        <div class="col-md-12 p-2 {{ $errors->has('nome') ? 'has-errors' : ''}}">
            <label class="control-label">Nome Completo</label>
            <input type="text" name="nome" value"{{ old('nome') }}" placeholder="Nome Completo" class="form-control" required>
            @if($errors->has('nome'))
              <span class="form-text">
                {{ $errors->first('nome') }}
              </span>
            @endif
        </div>
    </div>
    <div class="col-sm-4 p-2 {{ $errors->has('data_nascimento') ? 'has-errors' : ''}}">
        <label class="control-label">Data de Nascimento</label>
        <input type="date" name="data_nascimento" value"{{ old('data_nascimento') }}" placeholder="Data de Nascimento" class="form-control" max="2013-01-01" value="2013-01-01" required>
        @if($errors->has('data_nascimento'))
          <span class="form-text">
            {{ $errors->first('data_nascimento') }}
          </span>
        @endif
    </div>
    <div class="col-sm-2 p-2 {{ $errors->has('serie_ingresso') ? 'has-errors' : ''}}">
        <label class="control-label">Série de Ingresso</label>
        <input type="text" name="serie_ingresso" value"{{ old('serie_ingresso') }}" size="1" maxlength="1" pattern="[1-9]" placeholder="1° ao 9° Ano" class="form-control" onkeypress='return SomenteNumeroSemZero(event)' required>
        @if($errors->has('serie_ingresso'))
          <span class="form-text">
            {{ $errors->first('serie_ingresso') }}
          </span>
        @endif
    </div>
    <div class="col-md-12 p-2"><strong>Endereço</strong></div>
    <div class="col-md-2 p-2 {{ $errors->has('cep') ? 'has-errors' : ''}}">
        <label class="control-label">CEP</label>
        <input type="text" name="cep" id="cep" value"" size="9" maxlength="9" onblur="pesquisacep(this.value);" placeholder="CEP" class="form-control" onkeypress='return SomenteNumero(event)' required>
        @if($errors->has('cep'))
          <span class="form-text">
            {{ $errors->first('cep') }}
          </span>
        @endif
    </div>
    <div class="col-md-8 p-2 {{ $errors->has('rua') ? 'has-errors' : ''}}">
        <label class="control-label">Rua</label>
        <input type="text" name="rua" id="rua" value"{{ old('rua') }}" placeholder="Rua" class="form-control" readonly>
        @if($errors->has('rua'))
          <span class="form-text">
            {{ $errors->first('rua') }}
          </span>
        @endif
    </div>
    <div class="col-md-2 p-2 {{ $errors->has('numero') ? 'has-errors' : ''}}">
        <label class="control-label">Número</label>
        <input type="text" name="numero" value"{{ old('numero') }}" placeholder="Número" class="form-control" onkeypress='return SomenteNumero(event)' required>
        @if($errors->has('numero'))
          <span class="form-text">
            {{ $errors->first('numero') }}
          </span>
        @endif
    </div>
    <div class="col-md-3 p-2 {{ $errors->has('complemento') ? 'has-errors' : ''}}">
        <label class="control-label">Complemento</label>
        <input type="text" name="complemento" value"{{ old('complemento') }}" placeholder="Complemento" class="form-control">
        @if($errors->has('complemento'))
          <span class="form-text">
            {{ $errors->first('complemento') }}
          </span>
        @endif
    </div>
    <div class="col-md-5 p-2 {{ $errors->has('bairro') ? 'has-errors' : ''}}">
        <label class="control-label">Bairro</label>
        <input type="text" name="bairro" id="bairro"  value"{{ old('bairro') }}" placeholder="Bairro" class="form-control" readonly>
        @if($errors->has('bairro'))
          <span class="form-text">
            {{ $errors->first('bairro') }}
          </span>
        @endif
    </div>
    <div class="col-md-3 p-2 {{ $errors->has('cidade') ? 'has-errors' : ''}}">
        <label class="control-label">Cidade</label>
        <input type="text" name="cidade" id="cidade"  value"{{ old('cidade') }}" placeholder="Cidade" class="form-control" readonly>
        @if($errors->has('cidade'))
          <span class="form-text">
            {{ $errors->first('cidade') }}
          </span>
        @endif
    </div>
    <div class="col-md-1 p-2 {{ $errors->has('uf') ? 'has-errors' : ''}}">
        <label class="control-label">Estado</label>
        <input type="text" name="uf" id="uf"  value"{{ old('uf') }}" placeholder="Estado" class="form-control" size="2" readonly>
        @if($errors->has('uf'))
          <span class="form-text">
            {{ $errors->first('uf') }}
          </span>
        @endif
    </div>
    <div class="col-md-12 p-2"><strong>Dados da Mãe</strong></div>
    <div class="col-md-6 p-0">
        <div class="col-md-12 p-2 {{ $errors->has('nome_mae') ? 'has-errors' : ''}}">
            <label class="control-label">Nome Completo (Mãe)</label>
            <input type="text" name="nome_mae" value"{{ old('nome') }}" placeholder="Nome Completo - Mãe" class="form-control" required>
            @if($errors->has('nome_mae'))
              <span class="form-text">
                {{ $errors->first('nome_mae') }}
              </span>
            @endif
        </div>
    </div>
    <div class="col-sm-2 p-2 {{ $errors->has('cpf_mae') ? 'has-errors' : ''}}">
        <label class="control-label">CPF</label>
        <input type="text" name="cpf_mae" value"{{ old('cpf_mae') }}" placeholder="CPF" class="form-control" size="11" maxlength="11" onkeypress='return SomenteNumero(event)' required>
        @if($errors->has('cpf_mae'))
          <span class="form-text">
            {{ $errors->first('cpf_mae') }}
          </span>
        @endif
    </div>
    <div class="col-sm-4 p-2 {{ $errors->has('data_pagamento') ? 'has-errors' : ''}}">
        <label class="control-label">Data para Pagamento</label>
        <input type="date" name="data_pagamento" value"{{ old('data_pagamento') }}" placeholder="Data para Pagamento" class="form-control" id="data_min_hoje" required>
        @if($errors->has('data_pagamento'))
          <span class="form-text">
            {{ $errors->first('data_pagamento') }}
          </span>
        @endif
    </div>
    <button class="btn btn-info mt-2">SALVAR</button>
    </form>
</div>
@endsection
