@extends ("template.app")

@section("content")
<div class="row">
    <div class="col-md-6">
      <div class="col-md-12">
          <h3 class="mt-5">Deseja excluir <strong> {{ $estudantes['nome'] }} </strong> ?</h3>
          <div class="float-left">
              <a class="btn btn-xs btn-info" href="{{ url("/listar") }}">
                  <i class="fas fa-arrow-left"></i>
                  &nbsp;Cancelar
              </a>
              <a class="btn btn-xs btn-danger" href="{{ url("/$estudantes->id_estudantes/destroy") }}">
                  <i class="fas fa-user-slash"></i>
                  &nbsp;Excluir
              </a>
          </div>
      </div>
    </div>
    <div class="col-md-6 pt-2">
        <div class="card bg-defalt">
            <div class="card-header text-center bg-warning"> <strong> {{ $estudantes['nome'] }} </strong></div>
            <div class="card-body">
              <p><strong>Data de Nascimento: </strong> {{ $estudantes -> data2 }}</p>
              <p><strong>Sério de Ingresso: </strong> {{ $estudantes -> serie_ingresso }}</p>
              <p>Endereço</p>
              <p><strong>CEP: </strong> {{ $estudantes -> cep }}</p>
              <p><strong>Rua: </strong> {{ $estudantes -> rua }}</p>
              <p><strong>Número: </strong> {{ $estudantes -> numero }}</p>
              <p><strong>Complemento: </strong> {{ $estudantes -> complemento }}</p>
              <p><strong>Bairro: </strong> {{ $estudantes -> bairro }}</p>
              <p><strong>Cidade: </strong> {{ $estudantes -> cidade }}</p>
              <p><strong>Estado: </strong> {{ $estudantes -> uf }}</p>
              <p>Dados da Mãe</p>
              <p><strong>Nome da mãe: </strong> {{ $estudantes -> nome_mae }}</p>
              <p><strong>CPF da mãe: </strong> {{ $estudantes -> cpf_mae }}</p>
              <p><strong>Data de pagamento: </strong> {{ $estudantes -> data3 }}</p>
            </div>
        </div>
    </div>
</div>
@endsection
