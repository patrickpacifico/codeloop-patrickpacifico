@extends ("template.app")

@section("content")
<div class="col-md-12">
    <h3>Editar <strong> {{ $estudantes['nome'] }} </strong></h3>
</div>
<div class="row p-2">
  <div class="col-md-6 p-0">
    <form action="{{ url('/update') }}" method="POST">
      {{ csrf_field() }}
      <input type="hidden" name="id" value="{{ $estudantes['id_estudantes']}}">
      <div class="col-md-12 p-2 {{ $errors->has('nome') ? 'has-errors' : ''}}">
          <label class="control-label">Nome Completo</label>
          <input type="text" name="nome" value="{{ $estudantes['nome'] }}" placeholder="Nome Completo" class="form-control" required>
          @if($errors->has('nome'))
            <span class="form-text">
              {{ $errors->first('nome') }}
            </span>
          @endif
      </div>
  </div>
  <div class="col-sm-4 p-2 {{ $errors->has('data_nascimento') ? 'has-errors' : ''}}">
      <label class="control-label">Data de Nascimento</label>
      <input type="date" name="data_nascimento" value="{{ $estudantes['data_nascimento'] }}" placeholder="Data de Nascimento" class="form-control" max="2013-01-01" value="2013-01-01" required>
      @if($errors->has('data_nascimento'))
        <span class="form-text">
          {{ $errors->first('data_nascimento') }}
        </span>
      @endif
  </div>
  <div class="col-sm-2 p-2 {{ $errors->has('serie_ingresso') ? 'has-errors' : ''}}">
      <label class="control-label">Série de Ingresso</label>
      <input type="text" name="serie_ingresso" value="{{ $estudantes['serie_ingresso'] }}" size="1" maxlength="1" pattern="[1-9]" placeholder="1° ao 9° Ano" class="form-control" onkeypress='return SomenteNumeroSemZero(event)' required>
      @if($errors->has('serie_ingresso'))
        <span class="form-text">
          {{ $errors->first('serie_ingresso') }}
        </span>
      @endif
  </div>
  <div class="col-md-12 p-2"><strong>Endereço</strong></div>
  <div class="col-md-2 p-2 {{ $errors->has('cep') ? 'has-errors' : ''}}">
      <label class="control-label">CEP</label>
      <input type="text" name="cep" id="cep" value="{{ $estudantes['cep'] }}" size="9" maxlength="9" onblur="pesquisacep(this.value);" placeholder="CEP" class="form-control" onkeypress='return SomenteNumero(event)' required>
      @if($errors->has('cep'))
        <span class="form-text">
          {{ $errors->first('cep') }}
        </span>
      @endif
  </div>
  <div class="col-md-8 p-2 {{ $errors->has('rua') ? 'has-errors' : ''}}">
      <label class="control-label">Rua</label>
      <input type="text" name="rua" id="rua" value="{{ $estudantes['rua'] }}" placeholder="Rua" class="form-control" readonly>
      @if($errors->has('rua'))
        <span class="form-text">
          {{ $errors->first('rua') }}
        </span>
      @endif
  </div>
  <div class="col-md-2 p-2 {{ $errors->has('numero') ? 'has-errors' : ''}}">
      <label class="control-label">Número</label>
      <input type="text" name="numero" value="{{ $estudantes['numero'] }}" placeholder="Número" class="form-control" onkeypress='return SomenteNumero(event)' required>
      @if($errors->has('numero'))
        <span class="form-text">
          {{ $errors->first('numero') }}
        </span>
      @endif
  </div>
  <div class="col-md-3 p-2 {{ $errors->has('complemento') ? 'has-errors' : ''}}">
      <label class="control-label">Complemento</label>
      <input type="text" name="complemento" value="{{ $estudantes['complemento'] }}" placeholder="Complemento" class="form-control">
      @if($errors->has('complemento'))
        <span class="form-text">
          {{ $errors->first('complemento') }}
        </span>
      @endif
  </div>
  <div class="col-md-5 p-2 {{ $errors->has('bairro') ? 'has-errors' : ''}}">
      <label class="control-label">Bairro</label>
      <input type="text" name="bairro" id="bairro"  value="{{ $estudantes['bairro'] }}" placeholder="Bairro" class="form-control" readonly>
      @if($errors->has('bairro'))
        <span class="form-text">
          {{ $errors->first('bairro') }}
        </span>
      @endif
  </div>
  <div class="col-md-3 p-2 {{ $errors->has('cidade') ? 'has-errors' : ''}}">
      <label class="control-label">Cidade</label>
      <input type="text" name="cidade" id="cidade"  value="{{ $estudantes['cidade'] }}" placeholder="Cidade" class="form-control" readonly>
      @if($errors->has('cidade'))
        <span class="form-text">
          {{ $errors->first('cidade') }}
        </span>
      @endif
  </div>
  <div class="col-md-1 p-2 {{ $errors->has('uf') ? 'has-errors' : ''}}">
      <label class="control-label">Estado</label>
      <input type="text" name="uf" id="uf"  value="{{ $estudantes['uf'] }}" placeholder="Estado" class="form-control" size="2" readonly>
      @if($errors->has('uf'))
        <span class="form-text">
          {{ $errors->first('uf') }}
        </span>
      @endif
  </div>
  <div class="col-md-12 p-2"><strong>Dados da Mãe</strong></div>
  <div class="col-md-6 p-0">
      <div class="col-md-12 p-2 {{ $errors->has('nome_mae') ? 'has-errors' : ''}}">
          <label class="control-label">Nome Completo (Mãe)</label>
          <input type="text" name="nome_mae" value="{{ $estudantes['nome_mae'] }}" placeholder="Nome Completo - Mãe" class="form-control">
          @if($errors->has('nome_mae'))
            <span class="form-text">
              {{ $errors->first('nome_mae') }}
            </span>
          @endif
      </div>
  </div>
  <div class="col-sm-2 p-2 {{ $errors->has('cpf_mae') ? 'has-errors' : ''}}">
      <label class="control-label">CPF</label>
      <input type="text" name="cpf_mae" value="{{ $estudantes['cpf_mae'] }}" placeholder="CPF" class="form-control" size="11" maxlength="11" onkeypress='return SomenteNumero(event)' >
      @if($errors->has('cpf_mae'))
        <span class="form-text">
          {{ $errors->first('cpf_mae') }}
        </span>
      @endif
  </div>
  <div class="col-sm-4 p-2 {{ $errors->has('data_pagamento') ? 'has-errors' : ''}}">
      <label class="control-label">Data para Pagamento</label>
      <input type="date" name="data_pagamento" value="{{ $estudantes['data_pagamento'] }}" placeholder="Data para Pagamento" class="form-control" id="data_min_hoje">
      @if($errors->has('data_pagamento'))
        <span class="form-text">
          {{ $errors->first('data_pagamento') }}
        </span>
      @endif
  </div>
  <button class="btn btn-info mt-2 ml-2">SALVAR</button>
  </form>
</div>
<div class="col-md-12 m-2 mt-3">
        <div class="card bg-defalt">
            <div class="card-header text-center bg-info"> <strong> {{ $estudantes['nome'] }} </strong>
              <a href="{{ url("/$estudantes->id_estudantes/excluir") }}" class="btn btn-xs btn-info p-1 float-right">
                  <i class="fas fa-trash-alt"></i>
              </a>
            </div>
            <div class="card-body">
              <p><strong>Data de Nascimento: </strong> {{ $estudantes -> data2 }}</p>
              <p><strong>Sério de Ingresso: </strong> {{ $estudantes -> serie_ingresso }}</p>
              <p>Endereço</p>
              <p><strong>CEP: </strong> {{ $estudantes -> cep }}</p>
              <p><strong>Rua: </strong> {{ $estudantes -> rua }}</p>
              <p><strong>Número: </strong> {{ $estudantes -> numero }}</p>
              <p><strong>Complemento: </strong> {{ $estudantes -> complemento }}</p>
              <p><strong>Bairro: </strong> {{ $estudantes -> bairro }}</p>
              <p><strong>Cidade: </strong> {{ $estudantes -> cidade }}</p>
              <p><strong>Estado: </strong> {{ $estudantes -> uf }}</p>
              <p>Dados da Mãe</p>
              <p><strong>Nome da mãe: </strong> {{ $estudantes -> nome_mae }}</p>
              <p><strong>CPF da mãe: </strong> {{ $estudantes -> cpf_mae }}</p>
              <p><strong>Data de pagamento: </strong> {{ $estudantes -> data3 }}</p>
            </div>
        </div>
    </div>
</div>
@endsection
