@extends ("template.app")

@section("content")

<div class="row">
        <div class="col-md-12 p-1 ml-3 mr-3">
          <h2>Processo Seletivo - Web Developer  <u>Terceira Etapa</u> - <strong>Patrick Pacífico da Silva</strong></h2>
        </div>
        <div class="col-md-12 p-1 m-3">
          <h5>Desenvolver um sistema básico de cadastramento de estudantes.</h5>
          &nbsp;
          <h6>1. O usuário deve poder criar, editar e deletar alunos</h6>
          <h6>- No menu "Funções" -> "Cadastrar Novo Estudante" = (CRIAR)</h6>
          <h6>- No menu "Funções" -> "Lista de Estudantes Cadastrados" -> "Lápis" = (EDITAR)</h6>
          <h6>- No menu "Funções" -> "Lista de Estudantes Cadastrados" -> "Lixeira" = (DELETAR)</h6>
          <h6>- No menu "Funções" -> "Lista de Estudantes Cadastrados" -> "Lápis" -> "Lixeira" = (DELETAR)</h6>
          &nbsp;
          <h6>2. O usuário deve poder ver todos os alunos cadastrados</h6>
          <h6>- No menu "Funções" -> "Lista de Estudantes Cadastrados" = (ALL)</h6>
          &nbsp;
          <i><h6>OBS1: Para ingressar no 1° Ano o estudante deve ter no <strong>mínimo 6 anos</strong>!<h6>
          <h6>OBS2: A data de pagamento não poderá ser retroativa!</h6>
          <h6>OBS3: Após um CEP válido, a RUA, BAIRRO, CIDADE e ESTADO são preenchidos automaticamete!</h6></i>
          &nbsp;
          <h6>Desenvolvido com:</h6>
        </div>
        <div class="col-md-2 ml-3">
          <h6>* Laravel *</h6>
        </div>
        <div class="col-md-2 ml-3">
          <h6>* MySQL *</h6>
        </div>
        <div class="col-md-3 ml-3">
          <h6>* CSS 3, HTML e JavaScript *</h6>
        </div>
        <div class="col-md-2 ml-3 mb-4">
          <h6>* BootStrap *</h6>
        </div>
</div>
@endsection
