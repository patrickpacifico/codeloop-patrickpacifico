@extends ("template.app")

@section("content")

<div class="row">
    @foreach($estudantes as $estudantes)
        <div class="col-md-4 p-3">
            <div class="card bg-defalt">
              <div class="card-header text-center bg-success text-light"> <strong> {{ $estudantes -> nome }} </strong>
                <a href="{{ url("/$estudantes->id_estudantes/editar") }}" class="btn btn-xs btn-success p-1 float-left">
                    <i class="fas fa-pencil-alt"></i>
                </a>
                <a href="{{ url("/$estudantes->id_estudantes/excluir") }}" class="btn btn-xs btn-success p-1 float-right">
                    <i class="fas fa-trash-alt"></i>
                </a>
              </div>
              <div class="card-body">
                <p><strong>Data de Nascimento: </strong> {{ $estudantes -> data2 }}</p>
                <p><strong>Sério de Ingresso: </strong> {{ $estudantes -> serie_ingresso }}</p>
                <p>Endereço</p>
                <p><strong>CEP: </strong> {{ $estudantes -> cep }}</p>
                <p><strong>Rua: </strong> {{ $estudantes -> rua }}</p>
                <p><strong>Número: </strong> {{ $estudantes -> numero }}</p>
                <p><strong>Complemento: </strong> {{ $estudantes -> complemento }}</p>
                <p><strong>Bairro: </strong> {{ $estudantes -> bairro }}</p>
                <p><strong>Cidade: </strong> {{ $estudantes -> cidade }}</p>
                <p><strong>Estado: </strong> {{ $estudantes -> uf }}</p>
                <p>Dados da Mãe</p>
                <p><strong>Nome da mãe: </strong> {{ $estudantes -> nome_mae }}</p>
                <p><strong>CPF da mãe: </strong> {{ $estudantes -> cpf_mae }}</p>
                <p><strong>Data de pagamento: </strong> {{ $estudantes -> data3 }}</p>
              </div>
            </div>
        </div>
    @endforeach
</div>
@endsection
