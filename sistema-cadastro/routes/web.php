<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/novo', function () {
    return view('/create');
});

Route::get('/listar', 'EstudantesController@listarView');

Route::group(['prefix' => '/'], function () {
    Route::get('/', 'EstudantesController@index');
    Route::get('/novo', 'EstudantesController@novoView');
    Route::get('/{id_estudantes}/editar', 'EstudantesController@editarView');
    Route::get('/{id_estudantes}/excluir', 'EstudantesController@excluirView');
    Route::get('/{id_estudantes}/destroy', 'EstudantesController@destroy');
    Route::post('/store', 'EstudantesController@store');
    Route::post('/update', 'EstudantesController@update');
});
