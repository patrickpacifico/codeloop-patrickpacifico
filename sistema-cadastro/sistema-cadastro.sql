-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 16-Jan-2019 às 00:28
-- Versão do servidor: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistema-cadastro`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `estudantes`
--

CREATE TABLE `estudantes` (
  `id_estudantes` int(10) UNSIGNED NOT NULL,
  `nome` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data_nascimento` date NOT NULL,
  `serie_ingresso` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cep` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rua` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numero` int(11) NOT NULL,
  `complemento` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bairro` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cidade` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uf` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nome_mae` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cpf_mae` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data_pagamento` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `estudantes`
--

INSERT INTO `estudantes` (`id_estudantes`, `nome`, `data_nascimento`, `serie_ingresso`, `cep`, `rua`, `numero`, `complemento`, `bairro`, `cidade`, `uf`, `nome_mae`, `cpf_mae`, `data_pagamento`, `created_at`, `updated_at`) VALUES
(1, 'Patrick Pacífico', '2011-08-18', '5', '20541190', 'Rua Paula Brito', 333, 'Casa 05', 'Andaraí', 'Rio de Janeiro', 'RJ', 'Carla Regina Pacífico', '13776501707', '2019-01-31', '2019-01-16 01:21:23', '2019-01-16 01:25:48'),
(2, 'Nicolas Rorato', '2006-02-21', '9', '69316738', 'Rua Laura Pinheiro Maia', 12, 'Apt 15', 'Pintolândia', 'Boa Vista', 'RR', 'Luiza Neves de Castro', '15246398877', '2019-04-11', '2019-01-16 01:23:16', '2019-01-16 01:23:16'),
(3, 'Joel Olivera Castro', '1985-02-06', '3', '71573010', 'Quadra 29 Conjunto J', 66, '10', 'Paranoá', 'Brasília', 'DF', 'Pamela Moutillho', '54215784693', '2019-07-18', '2019-01-16 01:24:31', '2019-01-16 01:24:31'),
(4, 'Jonas Ayres', '1981-01-30', '7', '68375482', 'Rua Princesa Izabel', 457, 'Apt 5011', 'Liberdade', 'Altamira', 'PA', 'Lucia De Azevedo', '64587498562', '2019-02-22', '2019-01-16 01:27:33', '2019-01-16 01:27:33');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(8, '2019_01_15_152122_create_tabela_estudantes', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `estudantes`
--
ALTER TABLE `estudantes`
  ADD PRIMARY KEY (`id_estudantes`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `estudantes`
--
ALTER TABLE `estudantes`
  MODIFY `id_estudantes` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
